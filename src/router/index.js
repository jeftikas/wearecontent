import Vue from 'vue'
import VueRouter from 'vue-router'
// import Home from '../views/Home.vue'
// import BarraLateral from '../components/BarraLateral.vue'

Vue.use(VueRouter)

const routes = [
  { 
    //el path va a ser la ruta por la que nos vaos a dirigir 
    path: '/',
    name: 'TuPanel',
    component:  () => import(/* webpackChunkName: "about" */ '../pages/Tupanel.vue')
  },
  {
    path: '/notificaciones',
    name: 'NotificPage',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.La diferencia con el component del primer objeto es que este se llama aqui miestras que el otro se importa de la carpeta views
    component: () => import(/* webpackChunkName: "about" */ '../pages/Notificaciones.vue')
  },
  {
    path: '/pedidos',
    name: 'PedidosPage',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.La diferencia con el component del primer objeto es que este se llama aqui miestras que el otro se importa de la carpeta views
    component: () => import(/* webpackChunkName: "about" */ '../pages/Pedidos.vue')
  },
  {
    path: '/cuenta',
    name: 'CuentaPage',
    component: () => import('../pages/Cuenta.vue')
  },
  {
    path: '/calculadora',
    name: 'CalculadoraPage',
    component: () => import('../pages/Calculadora.vue')
  },
  {
    path: '/creditos',
    name: 'CréditosPage',
    component: () => import('../pages/Creditos.vue')
  },
  {
    path: '/analisis',
    name: 'AnalisisSeo',
    component: () => import('../pages/Analisis.vue')
  }
]

const router = new VueRouter({
  routes
})

export default router
